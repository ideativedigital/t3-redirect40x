<?php

use TYPO3\CMS\Core\Utility\ArrayUtility;

$extendedConfig = [
    'columns' => [
        'errorRedirectPageDefault' => [
            'label' => 'LLL:EXT:id_redirect40x/Resources/Private/Language/locallang_tca.xlf:ErrorHandler.errorRedirectPageDefault',
            'description' => 'LLL:EXT:id_redirect40x/Resources/Private/Language/locallang_tca.xlf:ErrorHandler.errorRedirectPageDefault.desc',
            'config' => [
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'blindLinkOptions' => 'file,mail,spec,folder'
                        ]
                    ]
                ],
                'renderType' => 'inputLink',
                'type' => 'input',
            ]
        ],
        'errorRedirectPageIfNotLogged' => [
            'label' => 'LLL:EXT:id_redirect40x/Resources/Private/Language/locallang_tca.xlf:ErrorHandler.errorRedirectPageIfNotLogged',
            'description' => 'LLL:EXT:id_redirect40x/Resources/Private/Language/locallang_tca.xlf:ErrorHandler.errorRedirectPageIfNotLogged.desc',
            'config' => [
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'blindLinkOptions' => 'file,mail,spec,folder'
                        ]
                    ]
                ],
                'renderType' => 'inputLink',
                'type' => 'input',
            ]
        ]
    ],
    'types' => [
        'PHP' => [
            'showitem' => ' --palette--;;general, errorPhpClassFQCN, --palette--;;extended_php'
        ]
    ],
    'palettes' => [
        'extended_php' => [
            'showitem' => 'errorRedirectPageDefault, errorRedirectPageIfNotLogged'
        ]
    ]
];

ArrayUtility::mergeRecursiveWithOverrule(
    $GLOBALS['SiteConfiguration']['site_errorhandling'],
    $extendedConfig
);
