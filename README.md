![](./Documentation/Images/IdeativeLogo.png)

# TYPO3 Extension "id-redirect40x"

The purpose of this extension is to provide better management of 403 errors.

If a user tries to reach a page protected (for a given FE usergroup), and is not
logged, by default TYPO3 allows you to display content from a given page. But if
a logged user tries to reach the same protected page, and is still not allowed
to access the content the user should have a different content, informing that
he is not authorized to access the content.

According to [HTTP/1.1 standard](https://tools.ietf.org/html/rfc7231) , this is
two different error status code :

-   401 Unauthorized : Which semantically means "unauthenticated", i.e. the
    user does not have the necessary credentials

-   403 Forbidden : The request was valid, but the server is refusing action.
    The user might not have the necessary permissions for a resource, or may
    need an account of some sort.

## How to configure this

### Create dedicated pages

First, for each site stored in TYPO3, you must create the dedicated pages that
will be used by the extension.

At least, two pages must be created :

1.  The page that will be displayed when an unidentified user tries to access a
    protected page. In most cases, this will be the page displaying the login
    form.

2.  The page that will be displayed when an identified user tries to access a
    protected page. In most cases, it will be a page displaying a simple text
    indicating that the user is not authorized to access this content.

### Configure site

For each site, you must configure the 403 error handling config as follow:

![Site Configuration](./Documentation/Images/SiteConfiguration.png)

## Making this extension better

You found a bug, You have a fix?

Don't hesitate to create an issue or push a pull request. Any help is really
welcome. Thanks.

Made with ❤︎ in [Carouge](https://goo.gl/maps/Tg3VRacHZYJ2) by
<https://www.ideative.ch>
