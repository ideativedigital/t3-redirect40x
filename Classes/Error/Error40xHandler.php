<?php

declare(strict_types=1);

namespace Ideative\IdRedirect40x\Error;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Error\PageErrorHandler\PageErrorHandlerInterface;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\PageAccessFailureReasons;


class Error40xHandler implements PageErrorHandlerInterface
{
    /**
     * Extension name
     */
    public const extensionName = 'id_redirect40x';

    /**
     * Current Url
     * For returnUrl value on login form
     * @var string
     */
    protected string $currentUrl;

    /**
     * Default Target Page
     * @var string
     */
    protected string $targetPageDefault;

    /**
     * Secondary Target Page
     * @var string
     */
    protected string $targetPageSecondary;

    /**
     * @var int
     */
    protected int $statusCode;

    /**
     * @var ServerRequestInterface
     */
    protected ServerRequestInterface $request;

    /**
     * @var string
     */
    protected string $reasonCode;

    /**
     * @var string
     */
    protected string $reasonMessage;

    /**
     * @var array
     */
    protected array $errorHandlerConfiguration;

    /**
     * Indicates if the FE user is logged or not
     * @var bool
     */
    protected bool $feUserLogged;

    /**
     * @throws \InvalidArgumentException
     */
    public function __construct(int $statusCode, array $configuration)
    {
        $this->statusCode = $statusCode;
        $this->errorHandlerConfiguration = $configuration;

        $this->setTargetPageDefault($this->errorHandlerConfiguration['errorRedirectPageDefault']);
        $this->setTargetPageSecondary($this->errorHandlerConfiguration['errorRedirectPageIfNotLogged']);
    }


    /**
     * @param ServerRequestInterface $request
     * @param string $message
     * @param array $reasons
     * @return ResponseInterface
     * @throws Exception
     */
    public function handlePageError(
        ServerRequestInterface $request,
        string $message,
        array $reasons = []
    ): ResponseInterface {
        $this->setCurrentUrl($request->getUri()->__toString());
        $this->setReasonCode($reasons['code']);
        $this->setReasonMessage($message);
        $this->setRequest($request);

        try {
            switch ($this->getReasonCode()) {
                // Access restrictions
                case PageAccessFailureReasons::ACCESS_DENIED_GENERAL:
                case PageAccessFailureReasons::ACCESS_DENIED_PAGE_NOT_RESOLVED:
                case PageAccessFailureReasons::ACCESS_DENIED_SUBSECTION_NOT_RESOLVED:
                case PageAccessFailureReasons::ACCESS_DENIED_HOST_PAGE_MISMATCH:
                case PageAccessFailureReasons::ACCESS_DENIED_INVALID_PAGETYPE:
                    // There is a user identified, but he has no access to the current page
                    if ($this->isFeUserLogged() && !empty($this->getTargetPageSecondary())) {
                        $this->redirectToNotAuthorizedPage();
                    } else {
                        $this->redirectToLoginPage();
                    }
                    break;
                default:
                    $this->redirectToPageNotFound();
                    break;
            }
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), 1291068569);
        }
    }

    /**
     * Redirects the visitor to the login page with a return argument to the current page
     *
     * @throws SiteNotFoundException|\TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException
     */
    protected function redirectToLoginPage(): void
    {
        $resolvedUrl = $this->resolveUrl($this->getRequest(), $this->getTargetPageSecondary());

        if (!empty($resolvedUrl)) {
            header('HTTP/1.0 303 See other');
            $this->debugHeader();
            header('Location: ' . $resolvedUrl . '?return_url=' . urlencode($this->getCurrentUrl()));
            die;
        }

        throw new \RuntimeException(
            'Redirect40x Extension : Default Target Error Page not defined for 403 error.',
            1508712301
        );
    }

    /**
     * Redirects the visitor to the login page with a return argument to the current page
     *
     * @throws SiteNotFoundException|\TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException
     */
    protected function redirectToNotAuthorizedPage(): void
    {
        $resolvedUrl = $this->resolveUrl($this->getRequest(), $this->getTargetPageDefault());

        if (!empty($resolvedUrl)) {
            header('HTTP/1.0 303 See other');
            $this->debugHeader();
            header('Location: ' . $resolvedUrl);
            die;
        }
        throw new \RuntimeException(
            'Redirect40x Extension : Default Target Error Page not defined for 403 error.',
            1508712301
        );
    }

    /**
     * Redirects the visitor to the 404 page
     *
     * @throws SiteNotFoundException|\TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException
     */
    protected function redirectToPageNotFound(): void
    {
        $resolvedUrl = $this->resolveUrl($this->getRequest(), $this->getTargetPageDefault());

        if (!empty($resolvedUrl)) {
            header('HTTP/1.0 303 See other');
            $this->debugHeader();
            header('Location: ' . $resolvedUrl);
            die;
        }
        throw new \RuntimeException(
            'Redirect40x Extension : Default Target Error Page not defined for 404 error.',
            1508712301
        );
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return array
     */
    public function getErrorHandlerConfiguration(): array
    {
        return $this->errorHandlerConfiguration;
    }

    /**
     * @param array $errorHandlerConfiguration
     */
    public function setErrorHandlerConfiguration(array $errorHandlerConfiguration): void
    {
        $this->errorHandlerConfiguration = $errorHandlerConfiguration;
    }

    /**
     * @return string
     */
    public function getCurrentUrl(): string
    {
        return $this->currentUrl;
    }

    /**
     * @param string $currentUrl
     */
    public function setCurrentUrl(string $currentUrl): void
    {
        $this->currentUrl = $currentUrl;
    }

    /**
     * @return string
     */
    public function getTargetPageDefault(): string
    {
        return $this->targetPageDefault;
    }

    /**
     * @param string $targetPageDefault
     */
    public function setTargetPageDefault(string $targetPageDefault): void
    {
        $this->targetPageDefault = $targetPageDefault;
    }

    /**
     * @return string
     */
    public function getTargetPageSecondary(): string
    {
        return $this->targetPageSecondary;
    }

    /**
     * @param string $targetPageSecondary
     */
    public function setTargetPageSecondary(string $targetPageSecondary): void

    {
        $this->targetPageSecondary = $targetPageSecondary;
    }

    /**
     * @return bool
     */
    public function isFeUserLogged(): bool
    {
        return $this->getRequest()->getAttribute('frontend.user')->getUserId() > 0;
    }

    /**
     * @return string
     */
    public function getReasonCode(): string
    {
        return $this->reasonCode;
    }

    /**
     * @param string $reasonCode
     */
    public function setReasonCode(string $reasonCode): void
    {
        $this->reasonCode = $reasonCode;
    }

    /**
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest(ServerRequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getReasonMessage(): string
    {
        return $this->reasonMessage;
    }

    /**
     * @param string $reasonMessage
     */
    public function setReasonMessage(string $reasonMessage): void
    {
        $this->reasonMessage = $reasonMessage;
    }

    /**
     * Additional Header sent in Development/* context
     * For debug purpose only
     */
    protected function debugHeader(): void
    {
        $context = Environment::getContext();
        if ($context->isDevelopment()) {
            $pageAccessFailureReasons = GeneralUtility::makeInstance(PageAccessFailureReasons::class);
            header('X-T3-Redirect40x-Reason: "' . $pageAccessFailureReasons->getMessageForReason($this->getReasonCode()) . '"');
        }
    }

    /**
     * Get the current extension configuration
     * @return array
     */
    public function getTyposcriptConfiguration(): array
    {
        return $this->getFeController()->config['config']['errorPages.'];
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getFeController(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * Resolve the URL (currently only page and external URL are supported)
     *
     * @param ServerRequestInterface $request
     * @param string $typoLinkUrl
     * @return string
     * @throws SiteNotFoundException
     * @throws \InvalidArgumentException|\TYPO3\CMS\Core\LinkHandling\Exception\UnknownLinkHandlerException
     */
    protected function resolveUrl(ServerRequestInterface $request, string $typoLinkUrl): string
    {
        $linkService = GeneralUtility::makeInstance(LinkService::class);
        $urlParams = $linkService->resolve($typoLinkUrl);
        if ($urlParams['type'] !== 'page' && $urlParams['type'] !== 'url') {
            throw new \InvalidArgumentException('PageContentErrorHandler can only handle TYPO3 urls of types "page" or "url"',
                1522826609);
        }
        if ($urlParams['type'] === 'url') {
            return $urlParams['url'];
        }

        $site = $request->getAttribute('site', null);
        if (!$site instanceof Site) {
            $site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId((int)$urlParams['pageuid']);
        }
        // Build Url
        return (string)$site->getRouter()->generateUri(
            (int)$urlParams['pageuid'],
            ['_language' => $request->getAttribute('language', null)]
        );
    }
}
